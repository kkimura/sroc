use clap::Clap;

#[derive(Clap, Debug)]
#[clap(
    name=clap::crate_name!(),
    version=clap::crate_version!(),
    author=clap::crate_authors!(),
    about=clap::crate_description!(),
)]
pub struct Opts {
    #[clap(subcommand)]
    pub service: Action,
}

#[derive(Clap, Debug)]
pub enum Action {
    #[clap(name = "start", about = "Start sroc")]
    #[clap(setting(clap::AppSettings::ColoredHelp))]
    START,
    #[clap(name = "stop", about = "Stop sroc")]
    #[clap(setting(clap::AppSettings::ColoredHelp))]
    STOP,
    #[clap(name = "status", about = "Display the status of sroc")]
    #[clap(setting(clap::AppSettings::ColoredHelp))]
    STATUS,
}