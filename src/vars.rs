use std::io::Write;
use std::fs;
use std::path::PathBuf;
use std::os::unix::fs::OpenOptionsExt;
use anyhow::Result;

pub struct PreferenceVars {
    ssl_vpn_var: SSLVPNVars,
}

impl PreferenceVars {
    pub fn get_options_of_oc(&self) -> Vec<String> {
        let mut opts: Vec<String> = Vec::new();

        // protocol juniper
        opts.push(format!("--protocol={}", self.ssl_vpn_var.juniper));

        // username
        if let Some(username) = &self.ssl_vpn_var.username {
            opts.push(format!("-u {}", username));
        }

        // make openconnect background process
        opts.push("-b".to_string());

        // set vpn script
        if let Some(cirds) = &self.ssl_vpn_var.cirds {
            let script = Self::generate_vpn_script_text(cirds);

            match self.create_vpn_script_file(&script) {
                Ok(_) => {
                    opts.push(format!("--script={}", self.ssl_vpn_var.vpn_script_file));
                },
                Err(_) => {
                    println!("{}: cannot create vpn script file...", self.ssl_vpn_var.vpn_script_file);
                }
            }
        }

        // SSL-VPN Server site
        opts.push(format!("{}", self.ssl_vpn_var.vpn_host));

        opts
    }

    fn create_vpn_script_file(&self, script: &String) -> Result<PathBuf> {
        let vpn_script_path = PathBuf::from(&self.ssl_vpn_var.vpn_script_file);
        if vpn_script_path.exists() {
            match fs::remove_file(&vpn_script_path) {
                Ok(_) => {},
                Err(why) => return Err(anyhow::anyhow!(format!("{:?}", why.kind()))),
            }
        }

        let mut file = fs::OpenOptions::new()
                .create(true)
                .write(true)
                .mode(0o755)
                .open(&vpn_script_path)?;

        match file.write_all(script.as_bytes()) {
            Ok(_) => {},
            Err(e) => return Err(anyhow::anyhow!(format!("{:?}: {}", vpn_script_path.to_str(), e))),
        };

        Ok(vpn_script_path)
    }

    fn generate_vpn_script_text(cidrs: &Vec<IPv4CIDR>) -> String {
        let mut sh_vars = vec![format!("export CISCO_SPLIT_INC={}", cidrs.len())];

        for (i, cidr) in cidrs.iter().enumerate() {
            sh_vars.push(format!("export CISCO_SPLIT_INC_{}_ADDR={}", i, cidr.address));
            sh_vars.push(format!("export CISCO_SPLIT_INC_{}_MASKLEN={}", i, cidr.network_length));
        }

        sh_vars.push("exec /usr/share/vpnc-scripts/vpnc-script".to_string());
        sh_vars.join("\n")
    }
}

impl PreferenceVars {
    pub fn new(
        ssl_vpn_var: SSLVPNVars,
    ) -> Self {
        Self {
            ssl_vpn_var,
        }
    }

    pub fn get_sslvpn_client_name(&self) -> &str {
        &self.ssl_vpn_var.sslvpn_client_name
    }
}

pub struct SSLVPNVars {
    sslvpn_client_name: String,
    juniper: String,
    username: Option<String>,
    vpn_host: String,
    cirds: Option<Vec<IPv4CIDR>>,
    vpn_script_file: String,
}

impl SSLVPNVars {
    pub fn new(
        sslvpn_client_name: String,
        juniper: String,
        username: Option<String>,
        vpn_host: String,
        cirds: Option<Vec<IPv4CIDR>>,
        vpn_script_file: String,
    ) -> Self {
        Self {
            sslvpn_client_name,
            juniper,
            username,
            vpn_host,
            cirds,
            vpn_script_file,
        }
    }
}

pub struct IPv4CIDR {
    address: String,
    network_length: u8,
}

impl IPv4CIDR {
    pub fn parse_cidr(cidr: &String) -> Result<IPv4CIDR> {
        let temp_vec = cidr.split('/').collect::<Vec<&str>>();
        let ipv4 = temp_vec[0].to_string();
        let max_prefix_len = temp_vec[1].parse::<u8>()?;
        let c = IPv4CIDR {
            address: ipv4,
            network_length: max_prefix_len,
        };
        Ok(c)
    }
}
