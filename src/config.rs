use std::{io::BufReader, io::Read};
use std::fs::File;
use std::path::Path;
use serde::{Serialize, Deserialize};
use anyhow::{Context, Result};
use url::Url;
use crate::vars::*;

#[derive(Serialize, Deserialize, Debug)]
pub struct RustocTOML {
    openconnect: OpenConnect,
}

impl RustocTOML {
    pub fn parse_sroc_toml(filepath_str: &str) -> Result<PreferenceVars> {
        let rst_toml: Self = Self::parse_toml_file(filepath_str)?;

        // parsing openconnect conf
        let vpn_host = match &rst_toml.openconnect.vpn_host {
            Some(h) => {
                Url::parse(&h)?;
                h.clone()
            },
            None => return Err(anyhow::anyhow!("set URL of vpn_host.")),
        };
        let vpns_path = "/tmp/.vpn_script".to_string();

        let slv = SSLVPNVars::new(
            String::from("openconnect"),
            String::from("nc"),
            rst_toml.openconnect.username.clone(),
            vpn_host,
            Self::parse_split_cidrs(&rst_toml),
            vpns_path,
        );

        Ok(
            PreferenceVars::new(
                slv,
            )
        )
    }

    fn parse_toml_file(filepath_str: &str) -> Result<Self> {
        let buf = Self::read_file(filepath_str)?;
        toml::from_str(&buf).context("this file is not toml format.")
    }

    fn read_file(filepath_str: &str) -> Result<String> {
        let pathbuf = Path::new(filepath_str);
        let file = match File::open(&pathbuf) {
            Ok(f) => f,
            Err(e) => return Err(anyhow::anyhow!(format!("{}: {}", filepath_str, e))),
        };
        let mut r = BufReader::new(file);
        let mut buf = String::new();
        r.read_to_string(&mut buf)?;
        Ok(buf)
    }

    fn parse_split_cidrs(conf: &Self) -> Option<Vec<IPv4CIDR>> {
        if let Some(s) = &conf.openconnect.split_ipv4cirds {
            let mut cidr_vec = Vec::new();
            for cidr_str in s.iter() {
                let cidr = match IPv4CIDR::parse_cidr(cidr_str) {
                    Ok(c) => c,
                    Err(e) => {
                        println!("waring: {:?}", e);
                        return None
                    },
                };
                cidr_vec.push(cidr);
            }
            Some(cidr_vec)
        } else {
            return None
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct OpenConnect {
    juniper: Option<String>,
    username: Option<String>,
    vpn_host: Option<String>,
    split_ipv4cirds: Option<Vec<String>>,
}
