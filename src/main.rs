use std::env;
use clap::Clap;
mod config;
mod cli;
mod cmd;
mod vars;


fn main() {
    let opts: cli::Opts = cli::Opts::parse();

    let conf_file_path = match env::var("SROC_CONFIG_PATH") {
        Ok(c) => c,
        Err(_) => {
            format!("{}/.sroc.toml", dirs::home_dir().unwrap().to_str().unwrap())
        },
    };

    let pref_vars = match config::RustocTOML::parse_sroc_toml(&conf_file_path) {
        Ok(c) => c,
        Err(e) => {
            eprintln!("{}", e);
            std::process::exit(2);
        },
    };

    let oc = match cmd::OpenConnect::new(&pref_vars) {
        Ok(o) => o,
        Err(e) => {
            eprintln!("{:?}", e);
            std::process::exit(1);
        },
    };

    match opts.service {
        cli::Action::START => {
            match oc.exec() {
                Ok(_) => (),
                Err(e) => {
                    eprintln!("{}", e);
                    std::process::exit(2);
                }
            }
        },
        cli::Action::STOP => {
            match oc.stop() {
                Ok(_) => (),
                Err(e) => {
                    eprintln!("{}", e);
                    std::process::exit(3);
                },
            }
        },
        cli::Action::STATUS => {
            println!("COMMAND    \t STATUS   \t PID");
            oc.print_status();
        },
    }
}