use anyhow::{Context, Result};
use crate::vars::*;

pub struct OpenConnect<'a> {
    preference_vars: &'a PreferenceVars,
}

impl<'a> OpenConnect<'a> {
    pub fn new(pref_vars: &'a PreferenceVars) -> Result<Self> {
        let proc_name = pref_vars.get_sslvpn_client_name();

        which::which(proc_name).context(format!("command not found: {}", proc_name))?;
        Ok(
            Self {
                preference_vars: pref_vars,
            }
        )
    }

    pub fn exec(&self) -> Result<()> {
        let proc_name = self.preference_vars.get_sslvpn_client_name();

        if let Some(_) = crate::cmd::get_pid(proc_name) {
            return Err(anyhow::anyhow!("{} already is working.", proc_name))
        }

        let options = self.preference_vars.get_options_of_oc();
        let mut cmd = runas::Command::new(proc_name);

        cmd.args(options.as_slice());
        cmd.status().context("failed")?;

        let ten_millis = std::time::Duration::from_millis(1500);
        std::thread::sleep(ten_millis);
        Ok(())
    }

    pub fn stop(&self) -> Result<()> {
        let proc_name = self.preference_vars.get_sslvpn_client_name();
        match crate::cmd::get_pid(proc_name) {
            Some(pid) => {
                let mut send_sigint = runas::Command::new("kill");
                send_sigint.arg("-INT").arg(format!("{}", pid));
                let c = send_sigint.status()?;
                if c.success() {
                    println!("stop {}", proc_name);
                } else {
                    return Err(anyhow::anyhow!("cannot stop {}", proc_name));
                }

                let ten_millis = std::time::Duration::from_millis(2000);
                std::thread::sleep(ten_millis);
                Ok(())
            },
            None => {
                Err(anyhow::anyhow!("{} halt.", proc_name))
            },
        }
    }

    pub fn print_status(&self) {
        let status_str = match crate::cmd::get_pid(self.preference_vars.get_sslvpn_client_name()) {
            Some(pid) => {
                format!("working\t {}", pid)
            },
            None => {
                format!("stop   \t")
            }
        };

        println!("{}\t {}", self.preference_vars.get_sslvpn_client_name(), status_str);
    }
}
