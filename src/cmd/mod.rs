use sysinfo::{ProcessExt, SystemExt};

mod openconnect;
pub use openconnect::OpenConnect;


fn get_pid(proc_name: &str) -> Option<i32> {
    let mut system = sysinfo::System::new_all();

    // First we update all information of our system struct.
    system.refresh_all();

    // Now let's print every process' id and name:
    for (pid, proc_) in system.get_processes() {
        if proc_.name().eq(proc_name) {
            return Some(*pid);
        }
    }
    None
}