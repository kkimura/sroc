# sroc
sroc  -  simple controller of openconnect

sroc may be used to introspect and control the state of openconnect.  
sroc can only be used with Juniper's Network Connect protocol.

## Requirements
sroc depends on OpenConnect VPN client on Ubuntu 18.04 (or 20.04).  
If you are a **Ubuntu 18.04 (or 20.04)** user, then you can install the package by the following command:
```
sudo apt install openconnect
```
openconnect requires privileges so that sroc also requires privileges.


## TOML configuration
You need to create the configuration file for sroc.  
sroc finds a configuration from the following paths:
- `$SROC_CONFIG_PATH`
- `$HOME/.sroc.toml`

Example `~/.sroc.toml`:
```
[openconnect]
username = "alice"
vpn_host = "httpt://vpn.sample.com"
split_ipv4cirds = [
    "14.0.0.0/8",
]
```
- `username`: username in SSL-VPN Server 
- `vpn_host`: the URL which you will connect to SSL-VPN Server
- `split_ipv4cirds`: IPv4 CIRDs of IPs that have to be accessed through the VPN tunnel   

If `split_ipv4cirds` contains at least one ipv4cird element, openconnect becomes Split tunnel mode.


## Usage
Start openconnect service.
```
sroc start
```

Stop openconnect service.
```
sroc stop
```

Display the status of openconnect service.
```
sroc status
```

## Installation
### From source
```
git clone 
cd sroc
cargo install --path .
```

### From binary


## License
This program is licensed under MIT license ([LICENSE](./LICENSE) or [https://opensource.org/licenses/MIT](https://opensource.org/licenses/MIT)).